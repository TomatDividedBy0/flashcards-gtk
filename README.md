# Flashcards CLI
Flashcards CLI is a work-in-progress Quizlet-style memorization activity which is handled entirely through the command-line interface. This means no account, no telemetry, no required internet connection, and it can run on near-anything. 

The program provides an exercise in which one is given various randomly ordered definitions pertaining to a topic and is required to enter their corresponding terms. During my time in school, I have found that performing this type of exercise repeatedly was incredibly effective in helping me prepare for exams. 

# Using the Program
This program runs in the command-line interface which means there's no graphics and all user-input is handled the by keyboard. Once I finish the program I plan to build proper executables for Mac, Linux, and Windows, but for the time being if you'd like to run it, go to the project directory and enter `python3 main.py`.

You will be greeted with the main menu when it runs, type `/h` when you do to see a list of menu commands and what they do.



# Working with Set Files

All the flashcard set files are JSONs contained within the `saved/` subdirectory as to ensure they can operate easily with Python's dict system. 

Tools exist within the program to create and edit these, but if you wish to edit manually, take the following into account:

- In order to ensure that the JSON opened corresponds to this program, there is a `isFlashcardSet` property which should read (boolean) `True`. If this does not exist, then the set will not open.
- There should be a `name` property which matches the file name exactly (minus the extension)
- A `count` property exists, which should match the number of questions in the set, presented as an integer
- `questions` is a nested dictionary, with each of its entries containing `term:definition` as its respective key-value pair

The program is designed to handle sets created within itself, any errors you run into during manual editing is something not currently planned to be checked for or fixed, so be careful to follow convention.


The icon for this program was made by Tomat0 and is licensed under CC-BY-NC-SA. 
