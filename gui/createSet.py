import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

class create_menu(Gtk.Window):
    def __init__(self):
    # Window itself
        # initialize window, variables, and elements
        super().__init__(title="GNOME Flashcards")
        self.set_default_size(800,600)
        frame = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        
        self.itemBox = Gtk.ListBox() # itemBox is the arrangement of itemRows
        self.itemBox.set_selection_mode(Gtk.SelectionMode.SINGLE)

        self.rowCount = 1 # Initializes to one row
        
        setTitle = Gtk.Label(label="EXAMPLE SET") # Name of set as inputted in the dialog
        termTitle = Gtk.Label(label="Term") # Indicates the term column
        defTitle = Gtk.Label(label="Definition") # Indicates the definition column
        
        # add in row elements
        frame.pack_start(self.itemBox, True, True, 0)
        #frame.add(setTitle)
        item = itemRow(self.rowCount)
        self.itemBox.add(item)
        # set up button events
        item.addButton.connect("clicked", self.rowAdd)

        self.add(frame)
    def rowAdd(self, button):
        self.rowCount += 1
        newItem = itemRow(self.rowCount)
        self.itemBox.add(newItem)
        print("Add pressed")        

class itemRow(Gtk.ListBoxRow):
    def __init__(self, rowCount):
        # initialize row elements
        super().__init__()
        self.rowLabel = Gtk.Label(label=str(rowCount)) # number of the row
        self.termInput = Gtk.Entry() # user input for the row's term
        self.defInput = Gtk.Entry() # user input for the row's definition
        self.addButton = Gtk.Button(label="add") # allows you to create a new row
        self.delButton = Gtk.Button(label="del") # allows you to delete a row
        self.swapButton = Gtk.Button(label="swap") #allows you to swap term and definition for a row

        # place row elements into row
        rowBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        rowBox.add(self.rowLabel)
        rowBox.add(self.termInput)
        rowBox.add(self.swapButton)
        rowBox.add(self.defInput)
        rowBox.add(self.addButton)
        rowBox.add(self.delButton)

        addButton.connect("clicked", self.rowAdd)

   def rowAdd(self, button):
        self.rowCount = 
        

win = create_menu()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
