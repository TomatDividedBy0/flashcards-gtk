import gi

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk


# Creates new card set based on user input
class NewFile(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Create new set...", transient_for=parent, flags=0)
        self.set_default_size(300,100)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK
        )
        # Initialize input prompts
        self.fileName = Gtk.Entry()
        self.setName = Gtk.Entry()
        # Inititalize labels for input prompts
        fileLabel = Gtk.Label("Enter filename (no extension):")
        setLabel = Gtk.Label(label="Enter title of set:")
        # Place items into window
        box = self.get_content_area()
        box.add(fileLabel)
        box.add(self.fileName)
        box.add(setLabel)
        box.add(self.setName)
        self.show_all()
# Opens existing card set based on user selection
class OpenFile(Gtk.FileChooserDialog):
    def __init__(self, parent):
        super().__init__(title="Please choose a file...", action=Gtk.FileChooserAction.OPEN)
        openButton = Gtk.Button(label="Open")
        cancelButton = Gtk.Button(label="Cancel")
        
        self.add_buttons(
            _Cancel, _Open, Gtk.ResponseType.OK,
        )
        # Set filters; one for card sets, one for wildcard
        filter_card = Gtk.FileFilter()
        filter_card.set_name("Flashcard files (.card)")
        filter_card.add_pattern("*.card")
        self.add_filter(filter_card)
             
        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        self.add_filter(filter_any)
