import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw

# Local imports (stored within gui/)
from gui.fileHandler import * # Import OpenFile and NewFile  

class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
    # Window itself
        super().__init__(*args, **kwargs)
        self.set_default_size(800,600)
        self.set_title("GTK Flashcards")
        grid = Gtk.Grid()
        empty = Gtk.Label()
        # initialize center buttons
        newButton = Gtk.Button(label="New")
        openButton = Gtk.Button(label="Open")
        # align them at center of screen
        center_buttons = Gtk.Grid()
        center_buttons.attach(newButton,0,0,1,1) # child, left, top, width, height
        center_buttons.attach(openButton,1,0,1,1)
        center_buttons.set_valign(Gtk.Align.CENTER)
        center_buttons.set_halign(Gtk.Align.CENTER)
        #link them to the event functions
        newButton.connect("clicked", self.new_clicked)
        openButton.connect("clicked", self.open_clicked)
        self.set_child(center_buttons)
    # Launch file chooser dialogue if "Open" is pressed
    def open_clicked(self, button):
        dialog = OpenFile(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            f = dialog.get_filename()
            print("File selected: " + f)
        elif response == Gtk.ResponseType.CANCEL:
            pass
        dialog.destroy()
        
    def new_clicked(self, button):
        # Launch NewFile dialog if "New" is pressed
        dialog = NewFile(self)
        response = dialog.run()
        # If ok, read text in input field
        if response == Gtk.ResponseType.OK:
            fname = dialog.fileName.get_text() + ".card"
            ftitle = dialog.setName.get_text()
            fpath = "saved/" + fname
            f = open(fpath, "x") 
        elif response == Gtk.ResponseType.CANCEL:
            pass
        dialog.destroy()

class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.GtkApplication")
app.run(sys.argv)
