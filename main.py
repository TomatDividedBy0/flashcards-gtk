import json
import os.path
import time
import random

# Program modes determine what screen the program should display while waiting in between or in response to input.
	# 0 is the command prompt, which controls the screens and is the default one when the program starts
	# 1 is the screen for exiting the program
	# 2 is the screen which allows a user to create a new set and configure it
	# 3 is the screen for opening a set for either editing or quizzing
	# 4 is the screen for configuring either the program or a specific set
	# 5 is the set for quizzing

programMode = 0  # 0 is the default mode


# Handles coloring the terminal text
# class color:
#   PURPLE = '\033[95m'
#   CYAN = '\033[96m'
#   DARKCYAN = '\033[36m'
#   BLUE = '\033[94m'
#   GREEN = '\033[92m'
#   YELLOW = '\033[93m'
#  RED = '\033[91m'
#  BOLD = '\033[1m'
#  UNDERLINE = '\033[4m'
#  END = '\033[0m'

# Beginning menu
print("flashcard-cli v0.1: Command-line flashcard utility.")

commandList = {"/h": "asks for help", "/q": "exits the program", "/n": "creates new flashcard set", "/o <filename>": "open existing set file for quizzing", "/e <filename>": "edit existing set file", "/c": "configure program settings"}   # stores the commands and their definitions

while True:  # stops program from prematurely exiting
	while programMode == 0:  # The command prompt screen
		commandPrompt = input("Enter command, /h for help.\n")

		if commandPrompt == "/h":  # help menu prints list of commands in a clean order, then returns back to command prompt
			programMode = 0
			helpString = "List of commands:"
			for i in range(0, len(commandList.keys())):
				helpString = helpString + "\n" + list(commandList)[i] + "\t" + commandList.get(list(commandList)[i])
			helpString = helpString + "\nAny questions, suggestions, or bug reports can be filed on the GitLab repository linked below:\nhttps://gitlab.com/TomatDividedBy0/flashcards-cli"
			print(helpString)
			time.sleep(1)
		elif commandPrompt == "/q":  # takes you to the exiting screen
			programMode = 1
		elif commandPrompt == "/n":  # takes you to the flashcard creation screen
			programMode = 2
			time.sleep(1)
		elif commandPrompt[0:2] == "/o" or commandPrompt[0:2] == "/e":  # first verifies if argument is valid, then redirects according to command
			if commandPrompt[-5:] == ".json":  # checks if user included the extension or not
				openPath = "saved/" + commandPrompt[3:]
			else:
				openPath = "saved/" + commandPrompt[3:] + ".json"

			if os.path.exists(openPath):
				if commandPrompt[0:3] == "/o ":
					with open(openPath) as tmpDict:
						activeSet = json.out(tmpDict)
					programMode = 3
				elif commandPrompt[0:3] == "/e ":
					with open(openPath) as tmpDict:
						activeSet = json.out(tmpDict)
					programMode = 4
				else:
					print("Something definitely went wrong here. Either try again or file a bug report.")
			elif (commandPrompt == "/o" or commandPrompt == "/o ") or (commandPrompt == "/e" or commandPrompt == "/e "):  # checks for missing args
				print("This command requires a filename as an argument. Try again.")
				time.sleep(1.5)
			elif commandPrompt[2] != " ":
				print("Command not recognized. Type /h to see a list of valid commands.")
				time.sleep(1.5)
			else:
				print(openPath + " does not exist. Check that you've entered it correctly.")
				time.sleep(1.5)
		else:
			print("Command not recognized. Type /h to see a list of valid commands.")

	while programMode == 1:  # The exiting screen
		print("Exiting...")
		quit()
	while programMode == 2:  # The flashcard set creation screen
		# creates new dict for all the set info, which will later be put into a json
		newSet = {"isFlashcardSet": True}  # Creates flashcard set represented by a dictionary
		# asks the filename of the set, forces retry if there already exists set with that name
		while True:
			promptName = input("What would you like to name the set? (Please note: the file extension will be added automatically)\n")
			fileName = "saved/" + promptName + ".json"
			if os.path.exists(fileName):
				print("There already exists a file with that name. Either delete the file and retry or choose a different name.")
				time.sleep(1.5)
				continue
			else:
				newSet["name"] = promptName
				break

		# asks how many questions should be in the set
		while True:  # will keep retrying until a valid integer is inputted for the set length
			try:
				promptLength = input("How many questions would you like for the set?\n")
				newSet["length"] = int(promptLength)  # converts the inputted set length from a string to an integer for future use, stores it in dict
				newSet["questions"] = {}  # creates a dict of the questions within the larger dict, stores it in dict
			except ValueError:
				print("That doesn't seem to be a valid number, please try again.")
				time.sleep(1.5)
				continue
			break
		# gives the user the ability to set each term and definition
		questionList = ""  # string to convert the question list to a string for use in the confirm prompt
		for q in range(1, newSet["length"] + 1):  # q represents the current question being edited; due to how python works, a for loop will only go to 1 under the upper limit, so a +1 has to be added to counteract that
			promptTerm = input("Question " + str(q) + " out of " + str(newSet["length"]) + ", please enter the term:\n")
			promptDefinition = input("Question " + str(q) + " out of " + str(newSet["length"]) + ", please enter the definition:\n")
			newSet["questions"][promptTerm] = [promptDefinition]

			questionList = questionList + "\t" + promptTerm + ": " + promptDefinition + "\n"

		# ask to confirm creation, if yes create and ask to open, if no, return back to menu, else retry
		print("Name:\t" + fileName[6:] + "\n" + "Question Count:\t" + str(newSet["length"]) + "\n" + questionList)
		while True:
			promptConfirm = input("\nCreate flashcard set file with this information? Type y for yes, n for no.\n")
			if promptConfirm == "y" or promptConfirm == "Y":
				with open(fileName, "w") as tmpDict:
					json.dump(newSet, tmpDict)
				print("Flashcard set created.")
				time.sleep(1)
				programMode = 0
				break
			elif promptConfirm == "n" or promptConfirm == "N":
				time.sleep(1)
				programMode = 0
				break
			else:
				print("Invalid input. Type y to create the set, type n to cancel.")
				time.sleep(1.5)
				continue
	while programMode == 3:  # The quizzing screen
		# card shuffling logic
		questions = activeSet["questions"]
		random.shuffle(questions)

		qCorrect = 0  # number of questions gotten correct
		for q in range(0, activeSet["length"]):
			currentQuestion = questions[q]
			guess = input("Question " + str(q+1) + " out of " + str(activeSet["length"]) + ", please enter the term which corresponds to the following definition:" + currentQuestion)
			if guess.lower() == currentQuestion["term"].lower():
				print("need to finish this, was testing how to extract term/definition")
	while programMode == 4:  # the editing screen
		print("Mode 4")


# stuff to add: colors, lines spanning terminal width
